<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2019 PrestaShop SA
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
* International Registered Trademark & Property of PrestaShop SA
**/

namespace PrestaShop\Module\PsTranslateYourModule\File;

class MoveFile
{
    const SANDBOX_PATH = _PS_CACHE_DIR_.'sandbox/';

    protected $filePath;
    protected $fileName;

    /**
     * __construct
     *
     * @param array $file
     *
     * @return void
     */
    public function __construct($file)
    {
        $this->setFilePath($file);
        $this->setFileName($file);
    }

    /**
     * Move file into PrestaShop sandbox's folder and return the path
     *
     * @return string|false
     */
    public function moveInPrestaShopSandbox()
    {
        if (!move_uploaded_file(
            $this->getFilePath(),
            self::SANDBOX_PATH . $this->getFileName()
        )) {
            return false;
        }

        return self::SANDBOX_PATH . $this->getFileName();
    }

    /**
     * setFilePath
     *
     * @param array $file
     *
     * @return void
     */
    public function setFilePath($file)
    {
        $this->filePath = $file['tmp_name'];
    }

    /**
     * getFilePath
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * setFileName
     *
     * @param array $file
     *
     * @return void
     */
    public function setFileName($file)
    {
        $this->fileName = $file['rename'];
    }

    /**
     * getFileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }
}
