<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2019 PrestaShop SA
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
* International Registered Trademark & Property of PrestaShop SA
**/

namespace PrestaShop\Module\PsTranslateYourModule\File;

class CheckFile
{
    const TRANSLATION_FILE_TYPE = '.php';
    const INITIAL_LANGUAGE = 'en';
    const INDEX_FILE_NAME = 'index';

    /**
     * Check if the file is correct.
     * It must be a translations file having for name format 'en.php'
     *
     * @param \SplFileInfo $file
     * @param string $fileName
     *
     * @return bool
     */
    public function isAllowedTranslationFile($file, $fileName)
    {
        // Extension must be 'php'
        if (self::TRANSLATION_FILE_TYPE !== substr($file, -4)) {
            return false;
        }

        // We don't take index file
        if (self::INITIAL_LANGUAGE === $fileName) {
            return false;
        }

        // Default language is EN, we don't need it
        if (self::INDEX_FILE_NAME === $fileName) {
            return false;
        }

        return true;
    }
}
