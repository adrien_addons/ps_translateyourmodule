{*
* 2007-2019 PrestaShop
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2019 PrestaShop SA
* @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
* International Registered Trademark & Property of PrestaShop SA
*}
<div id="drop_zone" class="zone-body">
    <div class="row">
        <div class="col-md-12">
            <form action="#" class="dropzone dz-clickable" id="importTranslationFile">
                <div class="loader"></div>
                <div class="import-start">
                    <i class="import-start-icon material-icons">cloud_upload</i><br>
                    <p class="import-start-main-text">
                        {l s='Drop xlsx file here or' mod='ps_translateyourmodule'} <a href="#" class="import-start-select-manual">{l s='select file' mod='ps_translateyourmodule'}</a>
                    </p>
                    <p class="import-start-footer-text">
                        {l s='Please upload one file at a time, .xlsx' mod='ps_translateyourmodule'}
                    </p>
                </div>
                <div class="import-failure">
                    <i class="import-failure-icon material-icons">error</i><br>
                    <p class="import-failure-msg">{l s='Oops... Upload failed.' mod='ps_translateyourmodule'}</p>
                    <a href="#" class="import-failure-details-action">{l s='What happened?' mod='ps_translateyourmodule'}</a>
                    <div class="import-failure-details">{l s='An error has occurred.' mod='ps_translateyourmodule'}</div>
                    <p>
                        <a class="import-failure-retry btn btn-tertiary" href="#">{l s='Try again' mod='ps_translateyourmodule'}</a>
                    </p>
                </div>
                <div class="import-success">
                    <i class="import-success-icon material-icons">done</i><br>
                    <p class="import-success-msg"></p>
                </div>
                <div class="dz-default dz-message"><span></span></div><input name="translationFile" type="file" class="dz-hidden-input" accept=".xlsx" style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
            </form>
        </div>
    </div>
</div>
