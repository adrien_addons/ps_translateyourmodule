{*
* 2007-2019 PrestaShop
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2019 PrestaShop SA
* @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
* International Registered Trademark & Property of PrestaShop SA
*}

{if false !== $postError}
<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {if 0 == $postError}
            {l s='Module name shouldn\'t be empty.' mod='ps_translateyourmodule'}
        {else if 1 == $postError}
            {l s='Not abled to create a ZIP with existing translations.' mod='ps_translateyourmodule'}
        {else if 2 == $postError}
            {l s='Not abled to export the translations.' mod='ps_translateyourmodule'}
        {else}
            {l s='An error occured' mod='ps_translateyourmodule'}
        {/if}
    </div>
</div>
{/if}